from flask import Flask, render_template, send_from_directory, url_for, request
from flask_uploads import UploadSet, AUDIO, configure_uploads
from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired, FileAllowed
from wtforms import SubmitField
from transcribe import audio_transcribe

app = Flask(__name__)
app.config['SECRET_KEY'] = 'thisissecret'
app.config['UPLOADED_AUDIOS_DEST'] = 'uploads'

audio = UploadSet('audios', AUDIO)
configure_uploads(app, audio)

class UploadForm(FlaskForm):
    media = FileField(
        validators=[
            FileAllowed(audio, 'Only audios are allowed'),
            FileRequired('File field should not be empty')
        ]
    )
    submit = SubmitField('Upload')

@app.route("/uploads/<filename>")
def get_file(filename):
    return send_from_directory(app.config['UPLOADED_AUDIOS_DEST'], filename)

@app.route("/results/<filename>")
def get_res(filename):
    return send_from_directory('output', filename)

@app.route('/download/<filename>')
def download(filename):
    return send_from_directory('output', filename, as_attachment=True)

@app.route('/transcribe/<filename>', methods=['POST'])
def convert(filename):
    file_path = app.config['UPLOADED_AUDIOS_DEST'] + "/" + filename
    filename = audio_transcribe(file_path)
    file_url = url_for('get_res', filename=filename)
    return render_template('results.html', file_url=file_url, filename=filename)

@app.route("/", methods=['GET','POST'])
def index():
    form = UploadForm()
    if form.validate_on_submit():
        filename = audio.save(form.media.data)
        file_url = url_for('get_file', filename=filename)
    else:
        file_url = None
        filename = None
    return render_template('index.html', form=form, file_url=file_url, filename=filename)

if __name__ == '__main__':
    app.run(debug=True)