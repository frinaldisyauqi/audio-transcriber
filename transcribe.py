import whisper
import os

def audio_transcribe(filename) :
    model = whisper.load_model("base")
    result = model.transcribe(filename)
    filename = filename.split('/')[-1]
    text = filename.split('.')[0]+".txt"
    with open('output/'+ text, 'w') as file:
        file.write(result['text'])
    return text