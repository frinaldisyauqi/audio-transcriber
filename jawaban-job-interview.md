# No 4
https://gitlab.com/frinaldisyauqi/audio-transcriber

# No 5
### Audio Transcriber Web Application
Aplikasi untuk menuliskan atau mengubah file audio menjadi teks
<br/>
Link YouTube : https://youtu.be/9_AGYQ9waAc
<br/>
![demo-image](docs/demo.gif)

# No 6

```mermaid
    graph TD
        A(fa:fa-file-audio Upload Audio) --> B
        B(Transcribe Audio) --> C
        C(Print Out Text Results)
```
Dataset latih merupakan gelombang suara dari audio-audio yang dilatih menggunakan neural network dimana memiliki layer encoder-decoder transformer architecture dengan sinusoidal positional encoding agar mesin dapat mempelajari grammar terhadap posisi atau sequence dari gelombang suara
